/**
 * Parses as much information as we can from an Erowid experience report.
 */
import ErowidReport from './ErowidReport';
import * as cheerio from 'cheerio';

class ErowidExperienceReportParser{
  async parse(text: string): Promise<ErowidReport> {
    const report = new ErowidReport();
    const $ = cheerio.load(text);
    report.title = $('div.title').text();
    report.author = $('div.author').text();
    report.author = report.author.replace('by ', '');
    report.substances = $('div.substance').text().replace(/\([^)]+\)/, '').split(/[,&]/).map((s: string) => s.trim());

    //Get the year
    const yearMatch = text.match(/Exp Year: (\d{4})/);
    if (yearMatch) {
      report.date = yearMatch[1];
    }

    //Get the gender
    const genderMatch = text.match(/Gender: ([a-zA-Z-_]+)/);
    if (genderMatch) {
      report.gender = genderMatch[1];
    }

    //Get the age
    const ageMatch = text.match(/Age at time of experience: (\d{1,3})/);
    if (ageMatch) {
      report.age = parseInt(ageMatch[1]);
    }

    //The actual experience
    const experienceMatch = text.match(/<!-- Start Body -->(.+)<!-- End Body -->/s);
    if (experienceMatch) {
      report.experience = experienceMatch[1];
      report.experience = report.experience.replace("<br>", '\n');
      report.experience = report.experience.replace(/<[^>]*>?/gm, ' ');
    }

    return report;
  }
}

export default ErowidExperienceReportParser;