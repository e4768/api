import ErowidReport from './ErowidReport';

class ErowidListing {
  private reports: ErowidReport[] = [];

  getReports(): ErowidReport[] {
    return this.reports;
  }

  setReports(reports: ErowidReport[]) {
    this.reports = reports;
  }

  addReport(report: ErowidReport) {
    this.reports.push(report);
  }
}

export default ErowidListing;
