class ErowidReport {
  public id: number;
  public title: string;
  public author: string;
  public substances: Array<string>;
  public experience: string;
  public date: string;
  public gender: string;
  public rating: number;
  public age: number;
}

export default ErowidReport;
