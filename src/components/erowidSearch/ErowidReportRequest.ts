import ErowidReport from './ErowidReport';
import ErowidExperienceReportParser from './ErowidExperienceReportParser';
import axios from 'axios';

class ErowidReportRequest {
  async getReport(id: number): Promise<ErowidReport> {
    const url = `http://www.erowid.org/experiences/exp.php?ID=${id}`;
    const response = await axios(url);
    const text = response.data;
    const parser = new ErowidExperienceReportParser();
    const report = await parser.parse(text);
    report.id = id;

    console.log('report', report);

    return report;
  }
}

export default ErowidReportRequest;