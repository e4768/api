import ReportQuery from '../experienceReport/ReportQuery';
import ErowidListing from './ErowidListing';
import {NumStringObject} from '../../types/default-object';
import axios from 'axios';
import ErowidSearchResultsParser from './ErowidSearchResultsParser';

class ErowidSearch {
  protected query: ReportQuery;

  constructor(query: ReportQuery) {
    this.query = query;
  }

  async search(limit = 10, offset = 0): Promise<ErowidListing> {
    const queryParams = this.getParamsForQuery();
    if (queryParams['Max'] === undefined && limit > 0) {
      queryParams['Max'] = limit;
      queryParams['Start'] = offset;
    }

    console.log('queryParams', queryParams);

    return this.makeSearchRequest(
      'https://erowid.org/experiences/exp.cgi?A=Search',
      queryParams
    );
  }

  protected async makeSearchRequest(url: string, queryParams: NumStringObject): Promise<ErowidListing> {
    const keys = Object.keys(queryParams);
    console.log('keys', keys);
    for (const key of keys) {
      url += '&' + key + '=' + queryParams[key];
    }
    console.log('search url', url);
    const result = await axios(url);
    const parser = new ErowidSearchResultsParser();
    return parser.parse(result.data);
  }

  protected getParamsForQuery(): NumStringObject {
    const queryParams: NumStringObject = {};
    if (this.query.getActivity()) {
      queryParams['S4'] = this.query.getActivity();
    }
    if (this.query.getGender()) {
      queryParams['GenderSelect'] = this.query.getGender();
    }
    if (this.query.getLanguage()) {
      queryParams['Lang'] = this.query.getLanguage();
    }
    if (this.query.getIntensity()) {
      const intensity = this.query.getIntensity();
      queryParams['Intensity'] = intensity.getMin();
      queryParams['I2'] = intensity.getMax();
    }

    //Substances
    if (this.query.getSubstance() !== undefined) {
      queryParams['S1'] = this.query.getSubstance();
    }
    if (this.query.getCombinationOne() !== undefined) {
      queryParams['S2'] = this.query.getCombinationOne();
    }
    if (this.query.getCombinationTwo() !== undefined) {
      queryParams['S3'] = this.query.getCombinationTwo();
    }

    if (this.query.getRoute()) {
      queryParams['DoseMethodID'] = this.query.getRoute();
    }

    if (this.query.getLimit() > 0) {
      queryParams['Max'] = this.query.getLimit();
    }

    return queryParams;
  }
}

export default ErowidSearch;
