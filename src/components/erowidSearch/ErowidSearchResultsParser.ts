import ErowidListing from './ErowidListing';
import * as cheerio from 'cheerio';
import ErowidReport from './ErowidReport';

class ErowidSearchResultsParser {
  parse(text: string): ErowidListing {
    const listing = new ErowidListing();

    const $ = cheerio.load(text);
    $('table.exp-list-table tr').each((i, elem) => {
      const $tr = $(elem);
      const $tds = $tr.children('td');
      if ($tds.length === 5) {
        const $rating = $tds[0];
        const $expId = $tds[1];
        const $user = $tds[2];
        const $substance = $tds[3];
        const $date = $tds[4];

        const erowidReport = new ErowidReport();
        erowidReport.author = $($user).text();
        erowidReport.date = $($date).text();
        erowidReport.substances = $($substance).text().replace(/\([^)]+\)/, '').split(/[,&]/).map((s) => s.trim());

        //Let's try to parse out the rating
        let rating = -1;
        const ratingHtml = $($rating).html();
        if (ratingHtml) {
          const match = ratingHtml.match(/exp_star_([0-9])/);
          if (match) {
            rating = parseInt(match[1]);
          }
        }
        erowidReport.rating = rating;

        //Let's try to parse out the experience id
        let expId = -1;
        if ($expId) {
          erowidReport.title = $expId.firstChild ? $($expId.firstChild).text() : '';
          const expIdHtml = $($expId).html();
          if (expIdHtml) {
            const match = expIdHtml.match(/ID=(\d+)/);
            if (match) {
              expId = parseInt(match[1]);
            }
          }
        }
        erowidReport.id = expId;
        listing.addReport(erowidReport);
      }
    });
    return listing;
  }
}

export default ErowidSearchResultsParser;