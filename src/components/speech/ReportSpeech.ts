import {TextToSpeechClient} from '@google-cloud/text-to-speech';
import {google} from '@google-cloud/text-to-speech/build/protos/protos';
import {ObjectId} from 'bson';

const textToSpeech = require('@google-cloud/text-to-speech');
const fs = require('fs');
const util = require('util');

class ReportSpeech {
  private client: TextToSpeechClient;

  constructor() {
    this.client = new textToSpeech.TextToSpeechClient({
      credentials: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        private_key: (process.env.GOOGLE_PRIVATE_KEY ?? '').replace(/\\n/g, '\n'),
        // eslint-disable-next-line @typescript-eslint/naming-convention
        client_email: process.env.GOOGLE_CLIENT_EMAIL,
      },
    });
  }

  async getAudioUrls(text: string): Promise<string[]> {
    const id = new ObjectId();
    const chunks: string[] = [];
    for (let i=0; i<text.length; i++) {
      let chunk = text.substring(i, i+5000);
      if (chunk.length === 5000) {
        //make sure we cutoff at a word boundary
        const lastSpace = chunk.lastIndexOf(' ');
        if (lastSpace > 0) {
          chunk = chunk.substring(0, lastSpace);
        }
      }
      chunks.push(chunk);
      i += chunk.length;
    }
    const audioUrls: string[] = [];
    const audioUrlPromises = chunks.map(async (chunk, index: number) => {
      const audioUrl = await this.getAudioUrl(chunk, id.toHexString(), index);
      audioUrls[index] = audioUrl;
    });
    await Promise.all(audioUrlPromises);
    return audioUrls;
  }

  async getAudioUrl(chunk: string, baseId: string, index: number): Promise<string> {
    // Construct the request
    const request: google.cloud.texttospeech.v1.ISynthesizeSpeechRequest = {
      input: {text: chunk},
      // Select the language and SSML voice gender (optional)
      voice: {languageCode: 'en-US', ssmlGender: 'NEUTRAL'},
      // select the type of audio encoding
      audioConfig: {audioEncoding:  'MP3'},
    };

    // Performs the text-to-speech request
    const [response] = await this.client.synthesizeSpeech(request);
    // Write the binary audio content to a local file
    const writeFile = util.promisify(fs.writeFile);
    const fileName = `${baseId}_${index}.mp3`;
    await writeFile(fileName, response.audioContent, 'binary');
    console.log('Audio content written to file: ' + fileName);
    return fileName;
  }
}

export default ReportSpeech;