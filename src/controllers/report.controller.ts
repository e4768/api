import {
  get, param,
  response,
  ResponseObject,
} from '@loopback/rest';
import ErowidReport from '../components/erowidSearch/ErowidReport';
import ErowidReportRequest from '../components/erowidSearch/ErowidReportRequest';

/**
 * OpenAPI response for ping()
 */
const ErowidReportResponse: ResponseObject = {
  description: 'Erowid Report',
  content: {
    'application/json': {
      schema: {
        type: 'object',
        title: 'ErowidReport',
        properties: {
          reports: ErowidReport,
        },
      },
    },
  },
};

/**
 * A simple controller to bounce back http requests
 */
export class ReportController {
  @get('/reports/{id}')
  @response(200, ErowidReportResponse)
  async report(@param.path.number('id') id: number): Promise<object> {
    const reportRequest = new ErowidReportRequest();
    return reportRequest.getReport(id);
  }
}
