import {
  post,
  response,
  ResponseObject, requestBody,
} from '@loopback/rest';
import ErowidSearch from '../components/erowidSearch/ErowidSearch';
import ErowidSearchRequest from '../models/ErowidSearchRequest';
import ReportQueryBuilder from '../components/experienceReport/ReportQueryBuilder';
import ReportQuery from '../components/experienceReport/ReportQuery';
import ErowidListing from '../components/erowidSearch/ErowidListing';

/**
 * OpenAPI response for ping()
 */
const ErowidReportListing: ResponseObject = {
  description: 'Erowid Search',
  content: {
    'application/json': {
      schema: {
        type: 'object',
        title: 'ErowidReportListing',
        properties: {
          reports: ErowidListing,
        },
      },
    },
  },
};

/**
 * A simple controller to bounce back http requests
 */
export class SearchController {
  // Map to `GET /ping`
  @post('/search')
  @response(200, ErowidReportListing)
  async search(@requestBody() search: ErowidSearchRequest): Promise<object> {
    // Reply with a greeting, the current time, the url, and request headers
    const reportQuery: ReportQuery = await new ReportQueryBuilder().fromText(search.search);
    const searcher = new ErowidSearch(reportQuery);
    return searcher.search(10);
  }
}
