import {
  get,
  response,
  ResponseObject,
  param,
} from '@loopback/rest';
import ErowidReportRequest from '../components/erowidSearch/ErowidReportRequest';
import ReportSpeech from '../components/speech/ReportSpeech';

/**
 * OpenAPI response for ping()
 */
const AudioUrlResponse: ResponseObject = {
  description: 'Erowid Search',
  content: {
    'application/json': {
      schema: {
        type: 'object',
        title: 'AudioUrlResponse',
        properties: {
          audioUrls: [{ type: 'string' }],
        },
      },
    },
  },
};

/**
 * A simple controller to bounce back http requests
 */
export class SpeechController {
  @get('/speech/{id}')
  @response(200, AudioUrlResponse)
  async speech(@param.path.number('id') id: number): Promise<object> {
    const reportRequest = new ErowidReportRequest();
    const report =  await reportRequest.getReport(id);
    const speech = new ReportSpeech();
    const audioUrls = await speech.getAudioUrls(report.experience);
    return { audioUrls };
  }
}
