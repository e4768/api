export type NumStringObject = {
  [key: string]: number | string;
};